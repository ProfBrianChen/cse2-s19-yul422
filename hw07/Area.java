import java.util.Scanner; 
//main class
public class Area{
   //main method
   public static void main(String []args){
      boolean k = true;
      String type = " ";
      //ask an input for the type of area
      while(k=true){ 
      Scanner myScanner = new Scanner( System.in );//accepy the input
      System.out.print("Input the type of area: rectangle circle or triangle");
      type = myScanner.next();
      if("rectangle".equals(type)){ 
          break;} // exist the loop when input is a valid type of shape
      if("triangle".equals(type)){ 
          break;}
      if("circle".equals(type)){ 
          break;}
       }
     
      if("rectangle".equals(type)){ 
          rectangle() ;} //run the method of rectangle calculation
      if("triangle".equals(type)){ 
          triangle();} //run the method of triangle calculation
      if("circle".equals(type)){ 
          circle();} //run the method of circle calculation
    }
  
   //method for check whether the input is a double 
   public static double check(){
     Scanner myScanner = new Scanner( System.in );
     //ask input until the input is valid
     while(!myScanner.hasNextDouble()){
           String junkWord = myScanner.next();
           System.out.print("Invalid input: input a double");
           }
         double input = myScanner.nextDouble();//declare the input
     return input;//return the input
   }
   //method for calculate the area of triangle
   public static void triangle(){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the base side of the triangle: ");
    double base = check(); //check whether the input is a double 
    System.out.print("Enter the height of the triangle: ");
    double height = check();//check whether the input is a double 
    double tarea;//declare the area
    tarea=base*height*0.5;
    System.out.println("the area of triangle is "+(tarea)+"");//print out the final answer
    }
  
   //method for calculate the area of rectangle
    public static void rectangle(){
     Scanner myScanner = new Scanner(System.in) ;
     System.out.println("Enter the width of the rectangle");
     double width = check();//check whether the input is a double 
     System.out.println("Enter the height of the rectangle");
     double height = check();
     double rarea;//declare the area
     rarea=height*width;//calculate the final value
     System.out.println("the area of rectangle is "+(rarea)+"");//print out the final answer
   }
   //method for calculate the area of circle
   public static void circle(){
   Scanner myScanner = new Scanner(System.in) ;
   System.out.println("Enter the radius of the circle");
   double r = check();//check whether the input is a double 
   double carea;
   carea=3.14*r*r;//declare the area
   System.out.println("the area of circle is "+(carea)+"");//print out the final answer
   }
   
  
  }