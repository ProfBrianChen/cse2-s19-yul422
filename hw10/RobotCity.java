import java.util.Arrays;
import java.util.Scanner; 

public class RobotCity{		
    public static void main(String[] args) {
      Scanner myscanner = new Scanner (System.in);
      System.out.println("input the number of robots");
      int k = myscanner.nextInt();  
      int[][] a = buildCity();
      System.out.println("Array");
      display(a);
      System.out.println("invade");
      int [][] b =invade(a,k);
      display(b);
      //update five times
      for(int i = 0; i < 5 ;i++){
      System.out.println("update" +(i+1));
      b=update(b);
      display(b);
      }
    }
   //generate a random array which dimension is 10 to 15
    public static int[][] buildCity(){
      int height = (int)(Math.random()*(5)+10);;
      int width = (int)(Math.random()*(5)+10);;
      int[][] cityArray = new int[height][width]; 
            int population = 100;
      //assign a random number between 100 and 999 to each city block
      for(int k = 0; k < height; k++){
        for(int a = 0; a < width; a++){
            cityArray[k][a]=(int)(Math.random()*100)+100;
            }
          }
        return cityArray;
      }
  
  
    //print the array
    public static void display(int[][] cityArray){
      for(int row = 0; row<cityArray.length; row++){           
         for(int column = 0; column < cityArray[row].length; column++){
             System.out.printf("%5d",cityArray[row][column]); 
              }
            System.out.println( " ") ;
            }       
     }
  
    public static int[][] invade(int[][] cityArray, int integer){
       int[][] kArray = new int[cityArray.length][cityArray[0].length];
       int k = 0;
       while(k<integer){
        //randomly generate block coordinates
        int height = (int)(Math.random()*(cityArray.length));
        int width = (int)(Math.random()*(cityArray[0].length));
        if(cityArray[height][width] > 0){
          //Set the integer to negative
           cityArray[height][width]=cityArray[height][width]*-1;
           k++;
           }
        }
      return cityArray;
    }
      
    public static int[][] update(int cityArray[][]){
      int[][] result = new int[cityArray.length][cityArray[0].length];
      //declare a new array 
      for(int k = 0; k < result.length; k++){
        for(int a = 0; a < result[0].length; a++){
            result[k][a]=cityArray[k][a];
            }
          }
      //moves the robots from their current block to the next block 
      for(int k = 0; k < result.length; k++){
        for(int a = 1; a < result[0].length; a++){
           if(cityArray[k][a-1]<0&&result[k][a]>0){
              result[k][a]=result[k][a]*-1;
              }
            }
         }

      
      return result;
    }
      
      
}
    
     