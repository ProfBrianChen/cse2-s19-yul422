import java.util.Arrays;
import java.util.Scanner; 

public class Straight{		
    public static void main(String[] args) {
          int r =0;
          int[]a=generate();
          int[]b=five(a);
          boolean k = straightk(b);
          for(int i = 0; i<1000000; i++){
              a=generate();
              b=five(a);
              k = straightk(b);
             if(k==true){
                 r=r+1;
                }
              }
           System.out.printf("%s",r);
           double u;
           u = r/1000000;
           System.out.println(" the chance is " +u);
    }
  
    public static int[] generate(){
      int [] deck = new int[52];
      int k=1;
      //give rach card a number from 1 to 52
      for (int i = 0; i < deck.length; i++) {
            deck[i] = k ;
            k++;
            }
         // Shuffle the cards
       for (int i = 0; i < deck.length; i++) {
            int index = (int)(Math.random() * deck.length);
            int temp = deck[i];
            deck[i] = deck[index];
            deck[index] = temp;
            }
       return deck;
      }

  public static int[] five(int[]deck){
     int [] firstfive = new int[5];
     for(int k = 0; k < firstfive.length; k++){
         firstfive[k]=deck[k];
         }
      return firstfive;
    }
  
   public static int search(int[]deck, int number){
     if(number < 0 || number > 5){
        return -1;
        }
     Arrays.sort(deck);
     int result = 0;
     for(int i = 0; i<deck.length ; i++){
       if (number == i){
           result = deck[i];
           }
         }
      return result;
    }
  
   //find whether the array is straight
   public static boolean straight(int[]deck){
     //set a variable to a theoretically impossible value
     int a = -1;
     // Automatically sort the array
     Arrays.sort(deck);
     //test 5 numbers in array
     for(int i = 0; i < deck.length; i++){
       //if a is -1, this is the first time through the for loop
       //if the card we are on equals the value of previous card+1
        if(a==-1 || (a+1)==(deck[i]%13)){
           a = (deck[i]%13);
           }
        else{
             return false;
             }
        }
      return true;
     }
  
   public static boolean straightk(int[]deck){
     int a = -1;
     for(int i = 0; i < deck.length; i++){
         int b = search(deck,i);
        if(a==-1 || (a+1)==(b%13)){
           a = (b%13);
           }
        else{
             return false;
             }
        }
      return true;
     }
}
