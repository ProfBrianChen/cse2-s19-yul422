HW 10 Rubric


RobotCity.java: 48/50

10/10: Compiles
10/10: display()
10/10: buildCity()
10/10: invade()
8/10: update()
   You're supposed to make the numbers that were negative, positive when you update it.


Straight.java: 37.5/50

12.5/12.5: Compiles
12.5/12.5: Able to generate a shuffled deck and select a hand
12.5/12.5: Able to find a straight
0/12.5: Correct percentage

Total: 85.5/100
