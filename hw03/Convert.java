// YuzheLiu 201902011 CSE2
import java.util.Scanner;//import scanner class
//main method required for every Java program
public class Convert{		
   			public static void main(String[] args) {
        Scanner myScanner = new Scanner( System.in ); //construct the instance of scanner 
        System.out.print("Enter the distance for meters: ");//prompt the user for the distance of meters
        double meter = myScanner.nextDouble(); /// accept user input 
        double inch;//create variable inch
        inch = meter * 39.3701;//convert meter to inch
        System.out.println(" "+(meter)+" meters is "+(inch)+" inches ");//print out the final result
         }  //end of main method   
  	} //end of class