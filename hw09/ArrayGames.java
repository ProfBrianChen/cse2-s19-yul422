import java.util.Arrays;
import java.util.Scanner; 
import java.util.ArrayList;

public class ArrayGames{		
  public static void main(String[] args) {
        Scanner myscanner = new Scanner (System.in);
        //asks the user if they want to run insert or shorten
        System.out.println("linear or a binary search: insert---1; shorten---2");
        int type = myscanner.nextInt(); 
        int[] a = generate();//generate new array a
        int[] b = generate();//generate new array b
        //generate a random number for shorten
        int k = (int)(Math.random()*(20));
        //run insert
        if(type == 1){
           System.out.print (" Input A:");
           print(a);//print out the members of an integer array input
           System.out.println (" ");
           System.out.print (" Input B:");
           print(b);//print out the members of an integer array input
           System.out.println (" ");
           System.out.print (" Output:");
           int[] c = insert(a,b);
          System.out.println(Arrays.toString(c));
        }
        //run shorten
        if(type == 2){
           System.out.print(" Input a:");
           print(a);//print out the members of an integer array input
           System.out.println (" ");
           System.out.print (" Input b:" +k);
           int[] c = shorten(a,k);
           System.out.println (" ");
           System.out.print (" Output:");
           System.out.println(Arrays.toString(c));//print array generate by method shorten
        }
  }
  //method to generate new array
  public static int[] generate(){
          int k = (int)(Math.random()*(10)) + 10;//generate a random number between 10 and 20
          //declare and allocate the new array
          int[] random = new int [k];
          //initializing data in an array
          for(int i=0; i<random.length;i++){
              random[i]=(int)(Math.random()*(30));
               }
         return random;
  }
  
   public static void print(int [] numbers){
           //print out the members of an integer array input
           for(int i=0; i<numbers.length; i++){
               System.out.print(""+numbers[i]+",");
               }
    }
  
  public static int[] insert(int[]a, int[]b){
                int r = (int)(Math.random()*(a.length));// select a random member of the first array
                int length = a.length+b.length;
                int count = 0;
                //declare and allocate the new array
                int[] result = new int [length];
                //initializing data in an array
                for(int i=0; i<r;i++){
                    result[i] = a[i];
                    count ++;
                    }
                //insert every member of the second array in the first array
                for(int k=0; k<b.length;k++){
                     result[count++]=b[k];
                     }
                for(int i=r;i<a.length;i++){
                    result[count++]=a[i];
                }
                return result;//produce a new array contain both arrays
                }
  
 public static int[] shorten(int[]a, int b){
            int [] another = new int[a.length-1];//declare new array which length is one member less than the input
            for(int k=0; k<a.length;k++){
               //check if the input integer actually refers to an index within the range of the input array’s length
               if(a[k]==b){
               for(int i=0,c=0;i<a.length-1;i++){
                   another[c++]=a[i];
                    }
                 return another;//return a new array  
                
                 }
              }
                  return a; //return the original array
                 }    
}