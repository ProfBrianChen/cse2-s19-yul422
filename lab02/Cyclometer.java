// YuzheLiu 20190201 CSE2

//
public class Cyclometer {
  // main method required for every java program
  public static void main(String[] args){
  //our input data
    int secTrip1=480; // the time of secTrip1 is 480 seconds
    int secTrip2=3220;// the time of secTrip2 is 3220 seconds
    int countsTrip1=1561;// the counts of countsTrip1 is 1561
    int countsTrip2=9037;// the counts of countsTrip2 is 9037
    double wheelDiameter=27.0,  // the value of wheelDiameter is 480
  	PI=3.14159, //the value of constant PI is 3.14159
  	feetPerMile=5280,  //the value of constant feetPerMile is 5280
  	inchesPerFoot=12,   //the value of constant inchesPerFoot is 12
  	secondsPerMinute=60;  //the value of constant secondsPerMinute is 60
	  double distanceTrip1, distanceTrip2,totalDistance;  // create variables distanceTrip1,distanceTrip2 and totalDistance
    // print out the time in minutes and counts 
    System.out.println("Trip 1 took "+ (secTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts."); 
	  System.out.println("Trip 2 took "+(secTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    //run the calculations; store the values；Calculatint distanceTrip1,distanceTrip2 and totalDistance
    distanceTrip1=countsTrip1*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
	  	//Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles.");
	  System.out.println("The total distance was "+totalDistance+" miles.");
   }// end of main method
} // end of class