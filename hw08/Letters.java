// YuzheLiu CSE2
import java.util.Arrays;
import java.util.Random;

//main method required for every Java program
public class Letters{		
  public static void main(String[] args) {
      char[] ch = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                    'w', 'x', 'y', 'z' };
      //generate an arbitary size
      int length = (int)(Math.random()*(48));;
      char[] c=new char[length];
      //generate a random
      Random random=new Random();
      System.out.print("Random character Array: ");
      //print out the random array
      for (int i = 0; i < length; i++) {
           c[i]=ch[random.nextInt(ch.length)];
           System.out.print(""+c[i]+"");
           }  
      System.out.println("");
      getAtoM(c);//call method finds all the upper and lowercase letters from A to M
      getNtoZ(c);//call method finds all the upper and lowercase letters from N to Z
      }

  //reand the arbitrary array as input and finds all the upper and lowercase letters from A to M 
  public static void getAtoM(char[] c){
     System.out.print("AtoM characters: ");
         for (int i = 0; i <c.length; i++){
           if( c[i]<='m' && 'a'<=c[i] ){ 
               System.out.print(""+c[i]+"");
               }  
           if( c[i]<='M' && 'A'<=c[i] ){ 
               System.out.print(""+c[i]+"");
               }  
          }
      System.out.println("");
         }
  //reand the arbitrary array as input and finds all the upper and lowercase letters from N to Z
  public static void getNtoZ(char[] c){
     System.out.print("NtoZ characters: ");
         for (int i = 0; i <c.length; i++){
           if( c[i]<='z' && 'n'<=c[i] ){ 
               System.out.print(""+c[i]+"");
               }  
           if( c[i]<='Z' && 'N'<=c[i] ){ 
               System.out.print(""+c[i]+"");
               }  
          }
      System.out.println("");
         }
  
  
  
  
  
}                                         