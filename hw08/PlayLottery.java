import java.util.Arrays;
import java.util.Scanner; 

public class PlayLottery{		
    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        System.out.println("enter five integers between 0 and 59: ");
        int[] numbers = new int[5];
        //take five inputs from users
        for(int i=0; i<numbers.length; i++){
            numbers[i] = input.nextInt(); 
            } 
         //print the numbers take from users
         System.out.print("The numbers you pick are : ");
         for(int i=0; i<numbers.length; i++){
             System.out.print(""+numbers[i]+",");
         }
         System.out.println (" ");
         System.out.print("The winning numbers are : ");
         //call method for generate a random array
         int[] c = numbersPicked();
         //print out the random array
         for(int i=0; i<numbers.length; i++){
             System.out.print(""+c[i]+",");
         }
         boolean b = userWins(numbers,c);
         System.out.println (" ");
         //print out the outcome
         if(b==true){
           System.out.println("You win ");
         }
         if(b==false){
           System.out.println("You lose ");
         }
     }
//generate a random series of 5 integers in the range of 0 to 59
public static int[] numbersPicked(){
        int[] winning = new int [5];
           for(int i=0; i<winning.length;i++){
               winning[i]=(int)(Math.random()*(60));
               }
        return winning;
}
  
//compare the users numbers in order to the random numbers in order
public static boolean userWins(int[] numbers, int[] winning){
          boolean same = true;
          for(int i=0;i<numbers.length;i++){
              if(winning[i]!=numbers[i]){
                  same = false;
                  break;
                  }
               else{same = true;}
               }
           return same;
          }
}