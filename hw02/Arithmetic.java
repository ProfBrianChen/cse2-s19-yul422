// YuzheLiu hw#02 CSE2
//
public class Arithmetic{
     // main method required for every java program
     public static void main(String[] args){
     //our input data
     int numPants = 3; //Number of pairs of pants 
     double pantsPrice = 34.98; //Cost per pair of pants
     int numShirts = 2;//Number of sweatshirts
     double shirtPrice = 24.99; //Cost per shirt
     int numBelts = 1;//Number of belts
     double beltCost = 33.99;//cost per belt
     double paSalesTax = 0.06;//the tax rate
     double pantscost, shirtscost,beltscost,pantstax,shirtstax,beltstax,totalcost,totaltax,totalpaid; // create variables
     pantscost=pantsPrice*numPants; //calculate total cost of pants
     shirtscost=numShirts*shirtPrice; //calculate total cost of shirts
     beltscost=beltCost*numBelts; //calculate total cost of belts
     System.out.println("Total cost of pants is "+(pantscost)+" "); //print out total cost of pants 
     System.out.println("Total cost of shirts is "+(shirtscost)+" "); //print out total cost of shirts
     System.out.println("Total cost of belts is "+(beltscost)+" "); //print out total cost of belts
     pantstax=paSalesTax*pantscost;//calculate total tax of pants
     shirtstax=paSalesTax*shirtscost;//calculate total tax of pants
     beltstax=paSalesTax*beltscost;//calculate total tax of pants
     System.out.println("Total tax of pants is "+(String.format("%.2f", pantstax-0.005))+" "); //print out total tax of pants 
     System.out.println("Total tax of shirts is "+(String.format("%.2f", shirtstax-0.005))+" "); //print out total tax of shirts  
     System.out.println("Total tax of belts is "+(String.format("%.2f", beltstax-0.005))+" ");//print out total tax of belts  
     totalcost=pantscost+shirtscost+beltscost;//calculate total cost of purchase before tax
     System.out.println("Total cost of purchase before tax is "+(totalcost)+" ");  //print out total cost of purchase before tax 
     totaltax=pantstax+shirtstax+beltstax;//calculate total sales tax
     System.out.println("Total sales tax is "+(String.format("%.2f", totaltax-0.005))+" ");//print out total sales tax   
     totalpaid=totalcost+totaltax;//calculate total paid for this transaction, including sales tax
     System.out.println("Total paid for this transaction, including sales tax is "+(String.format("%.2f", totalpaid-0.005))+" ");
     //print out total paid for this transaction, including sales tax
      }}