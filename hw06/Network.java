import java.util.Scanner;//import scanner class
public class Network //this line defines the class, all programs in java must have at least one 
{ //ask fot the value of height
    public static void main(String[] args) { 
        Scanner myScanner = new Scanner( System.in ); 
        int height=0; //declare the height
        System.out.print("Input your desired height");      
        while( height ==0 ) {
            if(myScanner.hasNextInt())//make sure the input is an integer
            {
                height = myScanner.nextInt();
                if (height <0 ){
                    height = 0; //ask user input again when height less than zero
                    System.out.print("input less than zero : input again");
                    myScanner.next(); 
                }
            }
            else//when input is not an integer
            {
                myScanner.next(); 
                //ask user input an integral angin because their previous one is not valid
                System.out.print("Invalid input: input an integer");
            }
        }	
        //ask for the value of width in the same way       
        int width=0;
        System.out.print("Input your desired width");      
        while( width ==0 ) {
            if(myScanner.hasNextInt())
            {
                width = myScanner.nextInt();
                if (width <0 ){
                    width = 0;
                    System.out.print("input less than zero : input again");
                    myScanner.next();
                }
            }
            else
            {
                myScanner.next();
                //ask user input an integral angin because their previous one is not valid
                System.out.print("Invalid input: input an integer");
            }
        }	
        //ask for the value of size in the same way  
        int size=0;
        System.out.print("Input the square size");      
        while( size ==0 ) {
            if(myScanner.hasNextInt())
            {
                size = myScanner.nextInt();
                if (size <0 ){
                    size = 0;
                    System.out.print("input less than zero : input again");
                    myScanner.next();
                }
            }
            else
            {
                myScanner.next();
                //ask user input an integral angin because their previous one is not valid
                System.out.print("Invalid input: input an integer");
            }
        }	
        //ask for the value of edge length in the same way  
        int elength=0;
        System.out.print("Input edge length");      
        while( elength ==0 ) {
            if(myScanner.hasNextInt())
            {
                elength = myScanner.nextInt();
                if (elength <0 ){
                    elength = 0;
                    System.out.print("input less than zero : input again");
                    myScanner.next();
                }
            }
            else
            {
                myScanner.next();
                //ask user input an integral angin because their previous one is not valid
                System.out.print("Invalid input: input an integer");
            }
        }	
        char one,two;//declare one, two
        //when size is odd  
        int i = 0;
        if(size%2==1){	
            for(i=0; i<height; i++){ //identify the total height
                if(i%(size+elength)==0 || i%(size+elength)==(size-1)){one='#';two='-';}//save the value for one and two
                else{one='|';two=' ';}

                if(i%(size+elength) < size) 
                    for(int j=0; j<=width; j++){ //the total width 
                        if(j%(size+elength)==0 || j%(size+elength)==(size-1)) {
                            System.out.print(one);	//print out the final value
                        }
                        if(j%(size+elength)>0 && j%(size+elength) < (size-1)) {
                            System.out.print(two); //print out the final value
                        }	
                        if(j%(size+elength) > (size-1)) { 
                            if(i%(size+elength)==(size/2)){ //one line when size is odd
                                System.out.print("-"); //print out edge between squares horizontally
                            }
                            else{ System.out.print(" "); //space between squares horizontally
                            }  
                        }
                    }
                System.out.println(); 
            }

            if(i%(size+elength) >= size){ //print out the edge vertically
                for(int n=0;n<=width;n++){
                    if( n%(size+elength)==(size/2)){
                        System.out.print("|");}//one line when size is oddd
                    else { System.out.print(" "); }
                } System.out.println();//enter an new line
            } 
        } 




        //when size if even 
        if(size%2==0){	
            for(i=0; i<height; i++){ //the total height
                if(i%(size+elength)==0 || i%(size+elength)==(size-1)){one='#';two='-';}//give value to one and two
                else{one='|';two=' ';} 

                if(i%(size+elength) < size){
                    for(int j=0; j<=width; j++){ //the totol width 
                        if(j%(size+elength)==0 || j%(size+elength)==(size-1)) {
                            System.out.print(one);	//print out square
                        }
                        if(j%(size+elength)>0 && j%(size+elength) < (size-1)) {
                            System.out.print(two); //print out square
                        }	
                        if(j%(size+elength) > (size-1)) { //print out the edge horizontally
                            if(i%(size+elength)==(size/2)||i%(size+elength)==(size/2-1)){//two edges when size is even 
                                System.out.print("-");
                            }
                            else{ System.out.print(" ");
                            }  
                        }
                    }
                    System.out.println(); //enter a new line
                }

                if(i%(size+elength) >= size){ //print out the edge vertically
                    for(int n=0;n<=width;n++){
                        if( n%(size+elength)==(size/2) || n%(size+elength)==(size/2-1)){
                            System.out.print("|");}
                        else { System.out.print(" "); }
                    } System.out.println(); 
                }
            } 		
        }	



    }
}
